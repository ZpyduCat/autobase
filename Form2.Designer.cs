﻿namespace WindowsFormsApp4
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.fioBox = new System.Windows.Forms.TextBox();
            this.pasCountBox = new System.Windows.Forms.TextBox();
            this.gCountBox = new System.Windows.Forms.TextBox();
            this.gWeightBox = new System.Windows.Forms.TextBox();
            this.startPosBox = new System.Windows.Forms.TextBox();
            this.endPosBox = new System.Windows.Forms.TextBox();
            this.phoneNumberBox = new System.Windows.Forms.TextBox();
            this.sendBtn = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.returnBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "ФИО";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(12, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(252, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "Количество пассажиров";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(12, 135);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 24);
            this.label3.TabIndex = 2;
            this.label3.Text = "Вес груза";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(12, 170);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(162, 24);
            this.label4.TabIndex = 3;
            this.label4.Text = "Пункт погрузки";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(12, 205);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(172, 24);
            this.label5.TabIndex = 4;
            this.label5.Text = "Пункт разгрузки";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(12, 100);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(187, 24);
            this.label6.TabIndex = 5;
            this.label6.Text = "Количество груза";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(12, 240);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(219, 24);
            this.label7.TabIndex = 6;
            this.label7.Text = "Контактный телефон";
            this.label7.Click += new System.EventHandler(this.Label7_Click);
            // 
            // fioBox
            // 
            this.fioBox.Location = new System.Drawing.Point(270, 35);
            this.fioBox.Name = "fioBox";
            this.fioBox.Size = new System.Drawing.Size(230, 20);
            this.fioBox.TabIndex = 7;
            // 
            // pasCountBox
            // 
            this.pasCountBox.Location = new System.Drawing.Point(270, 70);
            this.pasCountBox.Name = "pasCountBox";
            this.pasCountBox.Size = new System.Drawing.Size(230, 20);
            this.pasCountBox.TabIndex = 8;
            // 
            // gCountBox
            // 
            this.gCountBox.Location = new System.Drawing.Point(270, 105);
            this.gCountBox.Name = "gCountBox";
            this.gCountBox.Size = new System.Drawing.Size(230, 20);
            this.gCountBox.TabIndex = 8;
            // 
            // gWeightBox
            // 
            this.gWeightBox.Location = new System.Drawing.Point(270, 140);
            this.gWeightBox.Name = "gWeightBox";
            this.gWeightBox.Size = new System.Drawing.Size(230, 20);
            this.gWeightBox.TabIndex = 8;
            // 
            // startPosBox
            // 
            this.startPosBox.Location = new System.Drawing.Point(270, 175);
            this.startPosBox.Name = "startPosBox";
            this.startPosBox.Size = new System.Drawing.Size(230, 20);
            this.startPosBox.TabIndex = 8;
            // 
            // endPosBox
            // 
            this.endPosBox.Location = new System.Drawing.Point(270, 210);
            this.endPosBox.Name = "endPosBox";
            this.endPosBox.Size = new System.Drawing.Size(230, 20);
            this.endPosBox.TabIndex = 8;
            // 
            // phoneNumberBox
            // 
            this.phoneNumberBox.Location = new System.Drawing.Point(270, 245);
            this.phoneNumberBox.Name = "phoneNumberBox";
            this.phoneNumberBox.Size = new System.Drawing.Size(230, 20);
            this.phoneNumberBox.TabIndex = 8;
            // 
            // sendBtn
            // 
            this.sendBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sendBtn.Location = new System.Drawing.Point(16, 283);
            this.sendBtn.Name = "sendBtn";
            this.sendBtn.Size = new System.Drawing.Size(248, 26);
            this.sendBtn.TabIndex = 9;
            this.sendBtn.Text = "Отправить анкету";
            this.sendBtn.UseVisualStyleBackColor = true;
            this.sendBtn.Click += new System.EventHandler(this.SendBtn_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(178, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(178, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Обезательно заполнить все поля";
            // 
            // returnBtn
            // 
            this.returnBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.returnBtn.Location = new System.Drawing.Point(520, 35);
            this.returnBtn.Name = "returnBtn";
            this.returnBtn.Size = new System.Drawing.Size(108, 55);
            this.returnBtn.TabIndex = 11;
            this.returnBtn.Text = "Вернутся назад";
            this.returnBtn.UseVisualStyleBackColor = true;
            this.returnBtn.Click += new System.EventHandler(this.ReturnBtn_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(634, 321);
            this.Controls.Add(this.returnBtn);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.sendBtn);
            this.Controls.Add(this.phoneNumberBox);
            this.Controls.Add(this.endPosBox);
            this.Controls.Add(this.startPosBox);
            this.Controls.Add(this.gWeightBox);
            this.Controls.Add(this.gCountBox);
            this.Controls.Add(this.pasCountBox);
            this.Controls.Add(this.fioBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Form2";
            this.Text = "Анкета";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox fioBox;
        private System.Windows.Forms.TextBox pasCountBox;
        private System.Windows.Forms.TextBox gCountBox;
        private System.Windows.Forms.TextBox gWeightBox;
        private System.Windows.Forms.TextBox startPosBox;
        private System.Windows.Forms.TextBox endPosBox;
        private System.Windows.Forms.TextBox phoneNumberBox;
        private System.Windows.Forms.Button sendBtn;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button returnBtn;
    }
}