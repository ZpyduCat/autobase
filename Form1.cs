﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Hide();
            Form2 form = new Form2();
            form.Show();
        }
        private void closeBtn_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void aboutBtn_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Мы предоставляем транспорт и водителей своим клиентам для перевозки людей и груза. Вы можете заказать наши услуги в вкладке Анкета");
        }
    }
}
