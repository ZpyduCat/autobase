﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp4
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Label7_Click(object sender, EventArgs e)
        {

        }

        private void ReturnBtn_Click(object sender, EventArgs e)
        {
            Hide();
            Form1 form = new Form1();
            form.Show();
        }

        private void SendBtn_Click(object sender, EventArgs e)
        {
            bool error = false;
            if (fioBox.Text == "") error = true;
            if (pasCountBox.Text == "") error = true;
            if (gCountBox.Text == "") error = true;
            if (gWeightBox.Text == "") error = true;
            if (startPosBox.Text == "") error = true;
            if (endPosBox.Text == "") error = true;
            if (phoneNumberBox.Text == "") error = true;

            if(error) MessageBox.Show("Анкета не заполнена полностью, пожалуйста проверьте все поля.");
            else MessageBox.Show("Анкета успешно заполнена, ожидайте звонка оператора!");
        }
    }
}
